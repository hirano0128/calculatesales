package jp.alhinc.hirano_ryuya.culate_sales;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CalculateSales {

	static Map<String, String> branch = new TreeMap<String, String>();
	static Map<String, Long> sale = new TreeMap<String, Long>();
	static Map<String, Long> branchtotalamount = new TreeMap<String, Long>();
	static Map<String, String> commodity = new TreeMap<String, String>();
	static Map<String, Long> commoditytotalamount = new TreeMap<String, Long>();

	public static void main(String[] args) throws IOException {

		BufferedReader br =null;
		try {
			//支店定義ファイルを開く
			File file = new File(args[0],"barch.lst");
			//// ファイルを開く
			FileReader fr =new FileReader(file);
			br = new BufferedReader(fr);

			String line = br.readLine();

				// 文字列の分割
				String[] tmp = line.split(",", -1);

				// 支店情報の登録
				branch.put(tmp[0],tmp[1]);
				sale.put(tmp[0],(long) 0);

		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if(br != null) {
				try {
				br.close();
				} catch(IOException e) {
				System.out.println("クローズできません。");
				}

			}
		}
		BufferedReader br2 =null;
		try {
			//商品定義ファイルを開く
			File file2 = new File(args[0],"commodity.lst");
			// ファイルを開く
			FileReader fr2 =new FileReader(file2);
			br2 = new BufferedReader(fr2);

			String line2 = br2.readLine();

				// 文字列の分割
				String[] tmp2 = line2.split(",", -1);

				Map<String, String> commodity = new TreeMap<String, String>();

				//商品情報の登録
				commodity.put(tmp2[0],tmp2[1]);

		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if(br != null) {
				try {
				br.close();
				} catch(IOException e) {
				System.out.println("クローズできません。");
				}
			}
		}


		//売り上げファイルの設定
		List<File> saleFile = new ArrayList<File>();

		// フォルダを開く
		File dir3 = new File(args[0]);

		// フォルダに存在するファイルまたはフォルダの一覧を取得する
		File[] fileList = dir3.listFiles();

		// 連番チェック用リスト(int)
		List<Integer> number = new ArrayList<Integer>();


		for (File file : fileList) {
			// ファイルの拡張子が「.rcd」且つファイル名が数字8桁になっていることを確認する
			if ( file.getName().matches("^[0-9]{8}\\.rcd$") ) {
				// 存在するファイルのみ取得する
				if ( file.isFile() && file.exists() ) {
					saleFile.add(file);
					number.add( Integer.parseInt( file.getName().substring(0,8) ) );
				}
			}
		}
		// 昇順にソート
		Collections.sort(number);

		// 売上ファイルの情報を1行ずつ保存する
		List<String> line = new ArrayList<String>();


		for (int i = 0; i < saleFile.size(); i++) {
			// ファイルを開く
			FileReader fr3 = new FileReader( saleFile.get(i) );
			BufferedReader br3 = new BufferedReader(fr3);

			try {
				String tmp3;
				// 売上ファイルを1行ずつ読み込む
				while ( (tmp3 = br3.readLine()) != null ) {
					line.add(tmp3);
				}
				// 売上額を型変換する
				long sales = Long.parseLong(line.get(i));

				//売上額の集計
				if(sale.get(i) == line.get(i)) {
					sales = sale.get(i) + sales;
					sale,set(sales);
				}

				// 支店の合計金額の集計


				// リストのクリア
				line.clear();
			} finally {
				try {
					br3.close();
					} catch (IOException e) {

					}
			}
		}



	}

}

