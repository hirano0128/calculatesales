﻿package jp.alhinc.hirano_ryuya.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// 売上集計クラス
// 売上ファイルを読み込み、支店と商品それぞれの
//合計金額に売上額を加算する

abstract public class AddSale {
	/* クラス変数 */
	// 売上ファイルのリスト
	static List<File> saleFile = new ArrayList<File>();

	/* クラスメソッド */

	static boolean readSaleFile(String args) {
		try {
			// フォルダを開く
			File dir = new File(args);
			// フォルダの有無を確認する
			if ( !(dir.exists()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}
			// パスがフォルダを指していることを確認する
			if ( !(dir.isDirectory()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}

			// フォルダに存在するファイルまたはフォルダの一覧を取得する
			File[] fileList = dir.listFiles();

			// 連番チェック用リスト(int)
			List<Integer> number = new ArrayList<Integer>();

			// ファイル一覧から売上ファイルのみを取得する
			for (File file : fileList) {
				// ファイルの拡張子が「.rcd」且つファイル名が数字8桁になっていることを確認する
				if ( file.getName().matches("^[0-9]{8}\\.rcd$") ) {
					// 存在するファイルのみ取得する
					if ( file.isFile() && file.exists() ) {
						saleFile.add(file);
						number.add( Integer.parseInt( file.getName().substring(0,8) ) );
					}
				}
			}

			// 昇順にソート
			Collections.sort(number);

			// 売上ファイル名の連番チェック
			for (int i = 1; i < number.size(); i++) {
				if ( number.get(i) != number.get(i-1) + 1 ) {
					throw new MyException("売上ファイル名が連番になっていません");
				}
			}

			return true;
		} catch (NumberFormatException e) {
			System.out.println("売上ファイル名が連番になっていません");
			return false;
		} catch (MyException e) {
			System.out.println( e.getMessage() );
			return false;
		}
	}

	static boolean saleCollection(String args, List<Branch> branch, List<Commodity> commodity) {
		try {
			// 売上ファイルの読み込み
			if ( !(readSaleFile(args)) ) {
				return false;
			}

			// 売上ファイルの情報を1行ずつ保存する
			List<String> line = new ArrayList<String>();

			for (int i = 0; i < saleFile.size(); i++) {
				// ファイルを開く
				FileReader fr = new FileReader( saleFile.get(i) );
				BufferedReader br = new BufferedReader(fr);

				try {
					String tmp;
					// 売上ファイルを1行ずつ読み込む
					while ( (tmp = br.readLine()) != null ) {
						line.add(tmp);
					}
					// 売上ファイルが3行であることを確認する
					if ( line.size() != 3 ) {
						throw new MyException(saleFile.get(i).getName() + "のフォーマットが不正です");
					}

					// 売上額を型変換する
					long sale = Long.parseLong(line.get(2), 10);

					int branchCount = 0;	// 支店コードの不一致回数
					int commodityCount = 0;	// 商品コードの不一致回数

					/* 売上額の集計 */
					// 支店の合計金額の集計
					for (int j = 0; j < branch.size(); j++) {
						// 支店コードが一致することを確認する
						if ( branch.get(j).getCode().equals(line.get(0)) ) {
							// 支店の合計金額に加算
							branch.get(j).calculateTotalAmount(sale);
						} else {
							branchCount++;
						}

						// 合計金額が10桁を超えていないことを確認する
						if ( branch.get(j).getTotalAmount() > 9999999999L ) {
							throw new MyException("合計金額が10桁を超えました");
						}
					}
					// 商品の合計金額の集計
					for (int j = 0; j < commodity.size(); j++) {
						// 商品コードが一致することを確認する
						if ( commodity.get(j).getCode().equals(line.get(1)) ) {
							// 商品の合計金額に加算
							commodity.get(j).calculateTotalAmount(sale);
						} else {
							commodityCount++;
						}

						// 合計金額が10桁を超えていないことを確認する
						if ( commodity.get(j).getTotalAmount() > 9999999999L ) {
							throw new MyException("合計金額が10桁を超えました");
						}
					}

					// 支店コードが一致しなかった場合
					if ( branchCount == branch.size() ) {
						throw new MyException(saleFile.get(i).getName() + "の支店コードが不正です");
					}
					// 商品コードが一致しなかった場合
					if ( commodityCount == commodity.size() ) {
						throw new MyException(saleFile.get(i).getName() + "の商品コードが不正です");
					}

					// リストのクリア
					line.clear();
				} finally {
					br.close();	//メモリの開放
				}
			}

			return true;
		} catch (MyException e) {
			System.out.println( e.getMessage() );
			return false;
		} catch (NumberFormatException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
	}
}
