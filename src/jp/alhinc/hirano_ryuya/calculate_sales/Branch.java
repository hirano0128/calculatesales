package jp.alhinc.hirano_ryuya.calculate_sales;


 //支店クラス
 //支店定義ファイルから読み込んだ情報を保持するクラス

public class Branch {
	String code;		// 支店コード
	String name;		// 支店名
	long totalAmount;	// 合計金額

	Branch(String code, String name) {
		this.code = code;
		this.name = name;
		this.totalAmount = 0;
	}

	String getCode() {
		return this.code;
	}

	String getName() {
		return this.name;
	}

	long getTotalAmount() {
		return this.totalAmount;
	}

	void calculateTotalAmount(long sale) {
		this.totalAmount += sale;
	}

	String output() {
		return this.code + "," + this.name + "," + this.totalAmount;
	}
}
