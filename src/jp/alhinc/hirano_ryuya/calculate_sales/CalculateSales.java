﻿package jp.alhinc.hirano_ryuya.calculate_sales;

import java.util.ArrayList;
import java.util.List;

// 例外処理クラス
// 例外が起きたときに任意のメッセージをスローする

class MyException extends Exception {
	MyException(String message) {
		super(message);
	}
}

//売上集計システム
public class CalculateSales {
	public static void main(String[] args) {
		// コマンドライン引数チェック
		if ( args.length != 1 ) {
			System.out.println("予期せsぬエラーが発生しました");
			return;
		}

		// 支店情報のリスト
		List<Branch> branch = new ArrayList<Branch>();
		// 商品情報のリスト
		List<Commodity> commodity = new ArrayList<Commodity>();

		// 1.支店定義ファイルの読み込み
		if ( !(InputFile.readBranchFile(args[0], branch)) ) {
			return;
		}
		// 2.商品定義ファイルの読み込み
		if ( !(InputFile.readCommodityFile(args[0], commodity)) ) {
			return;
		}
		// 3.集計
		if ( !(AddSale.saleCollection(args[0], branch, commodity)) ) {
			return;
		}
		// 4.集計結果出力
		if ( !(OutputFile.writeFile(args[0], branch, commodity)) ) {
			return;
		}

		return;
	}
}
