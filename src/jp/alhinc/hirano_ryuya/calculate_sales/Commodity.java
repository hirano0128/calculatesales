package jp.alhinc.hirano_ryuya.calculate_sales;

 // 商品クラス
 // 商品定義ファイルから読み込んだ情報を保持するクラス


public class Commodity {
	String code;		// 商品コード
	String name;		// 商品名
	long totalAmount;	// 合計金額

	  // コンストラクタ
	Commodity(String code, String name) {
		this.code = code;
		this.name = name;
		this.totalAmount = 0;
	}


	// 商品コードを取得する
	String getCode() {
		return this.code;
	}


	// 商品名を取得する
	String getName() {
		return this.name;
	}

	// 合計金額を取得する
	long getTotalAmount() {
		return this.totalAmount;
	}

	// 合計金額の集計(合計金額の加算のみ)
	void calculateTotalAmount(long sale) {
		this.totalAmount += sale;
	}


	// インスタンス変数の出力
	String output() {
		return this.code + "," + this.name + "," + this.totalAmount;
	}
}
