﻿package jp.alhinc.hirano_ryuya.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


 // ファイル読み込みクラス
 // 支店定義ファイル及び商品定義ファイルを読み込み、
 // 支店クラス及び商品クラスのインスタンス生成する
abstract public class InputFile{

	// 支店定義ファイルの読み込み
	static boolean readBranchFile(String args, List<Branch> branch) {
		try {
			// フォルダを開く
			File dir = new File(args);
			// フォルダの有無を確認する
			if ( !(dir.exists()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}
			// パスがフォルダを指していることを確認する
			if ( !(dir.isDirectory()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}

			// 支店定義ファイルを開く
			File file = new File(dir, "branch.lst");
			// ファイルの有無を確認する
			if ( !(file.exists()) ) {
				throw new MyException("支店定義ファイルが存在しません");
			}
			// パスがファイルを指していることを確認する
			if ( !(file.isFile()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}

			// ファイルを開く
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			try {
				String str;		// 1行ずつ文字列を保持する
				// ファイルから1行ずつ読み込む
				while ( (str = br.readLine()) != null ) {
					// 文字列の分割
					String[] tmp = str.split(",", -1);

					// 分割した文字列が2つであることを確認する
					if ( tmp.length != 2 ) {
						throw new MyException("支店定義ファイルのフォーマットが不正です");
					}

					// 支店コードが3桁の数字であることを確認する
					if ( !(tmp[0].matches("^[0-9]{3}$")) ) {
						throw new MyException("支店定義ファイルのフォーマットが不正です");
					}

					// 支店情報の登録(支店オブジェクト)
					branch.add( new Branch(tmp[0], tmp[1]) );
				}
			} finally {
				br.close();	//メモリの開放
			}

			return true;
		} catch (NumberFormatException e) {
			System.out.println("支店定義ファイルのフォーマットが不正です");
			return false;
		} catch (MyException e) {
			System.out.println( e.getMessage() );
			return false;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
	}


	// 商品定義ファイルの読み込み
	static boolean readCommodityFile(String args, List<Commodity> commodity) {
		try {
			// フォルダを開く
			File dir = new File(args);
			// フォルダの有無を確認する
			if ( !(dir.exists()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}
			// パスがフォルダを指しているか確認する
			if ( !(dir.isDirectory()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}

			// 商品定義ファイルを開く
			File file = new File(dir, "commodity.lst");
			// ファイルの有無を確認する
			if ( !(file.exists()) ) {
				throw new MyException("商品定義ファイルが存在しません");
			}
			// パスがファイルを指していることを確認する
			if ( !(file.isFile()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}

			// ファイルを開く
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			try {
				String str;		// 1行ずつ文字列を保持する
				// ファイルから1行ずつ読み込む
				while ( (str = br.readLine()) != null ) {
					// 文字列の分割(第2引数:-1)
					String[] tmp = str.split(",", -1);

					// 分割した文字列が2つであることを確認する
					if ( tmp.length != 2 ) {
						throw new MyException("商品定義ファイルのフォーマットが不正です");
					}

					// 商品コードが8桁のアルファベットと数字であることを確認する
					if ( !(tmp[0].matches("^[A-Z a-z 0-9]{8}$")) ) {
						throw new MyException("商品定義ファイルのフォーマットが不正です");
					}

					// 支店情報の登録(支店オブジェクト)
					commodity.add( new Commodity(tmp[0], tmp[1]) );
				}
			} finally {
				br.close();	//メモリの開放
			}

			return true;
		} catch (MyException e) {
			System.out.println( e.getMessage() );
			return false;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
	}
}