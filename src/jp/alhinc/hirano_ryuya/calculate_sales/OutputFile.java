﻿package jp.alhinc.hirano_ryuya.calculate_sales;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


// 集計結果出力クラス
// 集計結果を降順にソートし、
// 既定のフォーマットでファイルに出力する
abstract public class OutputFile {

	// 集計結果ファイルの出力
	static boolean writeFile(String args, List<Branch> branch, List<Commodity> commodity) {
		// 支店別集計ファイルの出力
		if ( !(writeBranchFile(args, branch)) ) {
			return false;
		}
		// 商品別集計ファイルの出力
		if ( !(writeCommodityFile(args, commodity)) ) {
			return false;
		}

		return true;
	}

	// 支店別集計ファイルの出力
	static boolean writeBranchFile(String args, List<Branch> branch) {
		try {
			// フォルダを開く
			File dir = new File(args);
			// フォルダの有無を確認する
			if ( !(dir.exists()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}
			// パスがフォルダを指していることを確認する
			if ( !(dir.isDirectory()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}

			// 支店別集計ファイルを開く
			File file = new File(dir, "branch.out");
			// ファイルの有無を確認する
			if ( !(file.exists()) ) {
				// 無ければ新しく作成する
				file.createNewFile();
			}

			// 支店の合計金額を降順にソートする
			Collections.sort( branch, Comparator.comparing(Branch::getTotalAmount).reversed() );

			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);

			try {
				// 集計結果をファイルに出力する
				for (int i = 0; i < branch.size(); i++) {
					pw.println( branch.get(i).output() );
				}
			} finally {
				pw.close();	// メモリの開放
			}

			return true;
		} catch (MyException e) {
			System.out.println( e.getMessage() );
			return false;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
	}

	// 商品別集計ファイルの出力
	static boolean writeCommodityFile(String args, List<Commodity> commodity) {
		try {
			// フォルダを開く
			File dir = new File(args);
			// フォルダの有無を確認する
			if ( !(dir.exists()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}
			// パスがフォルダを指していることを確認する
			if ( !(dir.isDirectory()) ) {
				throw new MyException("予期せぬエラーが発生しました");
			}

			// 支店別集計ファイルを開く
			File file = new File(dir, "commodity.out");
			// ファイルの有無を確認する
			if ( !(file.exists()) ) {
				// 無ければ新しく作成する
				file.createNewFile();
			}

			// 商品の合計金額を降順にソートする
			Collections.sort( commodity, Comparator.comparing(Commodity::getTotalAmount).reversed() );

			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);

			try {
				// 集計結果をファイルに出力する
				for (int i = 0; i < commodity.size(); i++) {
					pw.println( commodity.get(i).output() );
				}
			} finally {
				pw.close();	//メモリの開放
			}

			return true;
		} catch (MyException e) {
			System.out.println( e.getMessage() );
			return false;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
	}
}
